const express = require('express');
const socketio = require('socket.io');
let app = express();

let documents = {};
let setupServer = () => {
    
    app.set('port', process.env.PORT || 3001);
    
    var server = app.listen(app.get('port'), () => {
          console.log("App is running at http://localhost:%s in %s mode", app.get('port'), app.get('env'));
          console.log('Press CTRL-C to stop\n');
      });
  
      var io = socketio(server);
  
      io.on('connection' ,(socket) => {
          console.log('connection established with client');
          let previousId;
          const safeJoin = (currentId) => {
            socket.leave(previousId);
            socket.join(currentId);
            previousId = currentId;
          };

          socket.on('getDoc',(input) => {
              safeJoin(input);
              socket.emit("document", documents[input]); // io.emit ???   // send doc to client
          })

          socket.on("addDoc", (doc) => {
            documents[doc.id] = doc;
            safeJoin(doc.id);
            io.emit("documents", Object.keys(documents));   // broadcast to everyone that new document is available
            socket.emit("document", doc);                  // sends back data to only the initiator  
          });

          socket.on("editDoc", (doc) => {
            documents[doc.id] = doc;
            socket.to(doc.id).emit("document", doc);        //?????? 
          });
        
          io.emit("documents", Object.keys(documents));
  
          socket.on('disconnect', () => {
              console.log('Client disconnected');
          });
      })
  
  }
  
  setupServer();
  
  module.exports = app;