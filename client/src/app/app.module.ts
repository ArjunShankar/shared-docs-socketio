import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DocumentListComponent } from './components/document-list/document-list.component';
import { DocumentComponent } from './components/document/document.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { DocumentService } from './services/document.service';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DocumentListComponent,
    DocumentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SocketIoModule.forRoot({      // this object os of type 'SocketIoConfig'
      url: 'http://localhost:3001',
      options: {}
    })
  ],
  providers: [DocumentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
