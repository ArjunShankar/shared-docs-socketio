import {Component, OnDestroy, OnInit} from '@angular/core';
import {DocumentService} from '../../services/document.service';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy {

  docSub: Subscription;
  documents$: Observable<string[]>;
  currentDoc: string;

  constructor(
    private docService: DocumentService
  ) { }

  ngOnInit() {
    this.documents$ = this.docService.documents;
    this.docSub = this.docService.currentDocument.subscribe((doc) => {
      this.currentDoc = doc.id;
    });
  }

  ngOnDestroy() {
    this.docSub.unsubscribe();
  }

  loadDoc(id: string) {
    this.docService.getDocument(id);
  }

  newDoc() {
    this.docService.newDocument();
  }

}
